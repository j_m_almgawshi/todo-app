import 'package:flutter/material.dart';
import 'package:j_todo_app/ui/screens/user_type_screen.dart';

void main() => runApp(AppBase());

class AppBase extends StatelessWidget {

  @override
  Widget build(BuildContext context) => MaterialApp(
    debugShowCheckedModeBanner: false,
    home: UserTypeScreen()
  );
}