import 'package:flutter/material.dart';
import 'package:j_todo_app/ui/screens/todo_screen.dart';

class UserTypeButton extends StatelessWidget {
  final icon;
  const UserTypeButton({@required this.icon});

  @override
  Widget build(BuildContext context) => 
    FlatButton(
      padding: EdgeInsets.symmetric(horizontal: 50, vertical: 10),
      color: Colors.red,
      splashColor: Colors.redAccent,
      child: Icon(
        this.icon,
        size: 30.0,
        color: Colors.white
      ),
      onPressed: () {
        Navigator.of(context).push(MaterialPageRoute(builder: (context) => ToDoScreen()));
      },
  );
}