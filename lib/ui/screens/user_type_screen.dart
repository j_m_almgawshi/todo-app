import 'package:flutter/material.dart';
import 'package:j_todo_app/ui/widgets/user_type_button.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class UserTypeScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) => Scaffold(
    body: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset('assets/logo3.jpg'),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              UserTypeButton(icon: FontAwesomeIcons.userGraduate),
              UserTypeButton(icon: FontAwesomeIcons.chalkboardTeacher)
            ]
          )
        ]
      )
      )
  );
}