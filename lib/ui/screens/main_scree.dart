import 'package:flutter/material.dart';

class MainScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context)  => Scaffold(
    appBar: AppBar(
      title: Text('Todowesome'),
      actions: [
        IconButton(
          icon: Icon(Icons.dehaze),
          onPressed: () {},
        )
    ]
  ),
  body: Center(
    child: Column(
      children: [
        Image.asset('assets/logo.PNG')
      ]
    )
  )
  );
}