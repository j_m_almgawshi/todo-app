import 'package:flutter/material.dart';
import 'package:j_todo_app/ui/widgets/custom_dialog.dart';

enum Priority{
  important,
  medium,
  useless,
  none,
}

class DummyTask{
  final String title;
  final String body;
  bool isChecked;
  final Priority priority;

  Color getColor() => {
    Priority.important: Colors.red,
    Priority.medium: Colors.blue,
    Priority.useless: Colors.yellow,
    Priority.none: Colors.grey,
  }[this.priority]; 

  DummyTask({@required this.title, @required this.body, this.isChecked = false, this.priority = Priority.none});
}

class ToDoScreen extends StatefulWidget {

  @override
  _ToDoScreenState createState() => _ToDoScreenState();
}

class _ToDoScreenState extends State<ToDoScreen> {
  final tasks = [
    DummyTask(title: 'Task 1', body: 'Body 1, Body 1, Body 1, Body 1, Body 1, Body 1, Body 1, Body 1, Body 1, Body 1, Body 1, Body 1, Body 1, Body 1, Body 1, Body 1, Body 1, Body 1, Body 1, Body 1, Body 1, Body 1, Body 1'),
    DummyTask(title: 'Task 2', body: 'Body 2'),
    DummyTask(title: 'Task 3', body: 'Body 3', isChecked: true),
    DummyTask(title: 'Task 4', body: 'Body 4', priority: Priority.useless),
    DummyTask(title: 'Task 5', body: 'Body 5', priority: Priority.important),
  ];

  final bodyController = TextEditingController();



  @override
  Widget build(BuildContext context) => Scaffold(
    appBar: AppBar(      
      title: Text('To Do List'),
      backgroundColor: Colors.redAccent,
    ),
    floatingActionButton: FloatingActionButton(
      onPressed: (){},
      child: Icon(Icons.add) ,
      backgroundColor: Colors.redAccent),
    drawer: Drawer(
      child: ListView(
        children: <Widget>[
          DrawerHeader(
            child: Text('ToDowesome'),
            decoration: BoxDecoration(
              color: Colors.redAccent ),
          ),
          ListTile(
            title: Text('Your To Do'),
            leading: Icon(Icons.format_list_bulleted),
            onTap: () {} ),
          ListTile(
            title: Text('Your Feed Back'),
            leading: Icon(Icons.feedback),
            onTap: () {} ),
          ListTile(
            title: Text('Log Out'),
            leading: Icon(Icons.dashboard),
            onTap: () {} ), 
          ListTile( 
            title: Text('About'),
            leading: Icon(Icons.contacts),
            onTap: () {}  ), 
        ],
      ), 
    ), 
    body: ReorderableListView(
      onReorder: (oldindex, newIndex){},
      children: [
        ...tasks.map(
          (task) => Card(
            key: Key(task.title),
            child: InkWell(
              child: ListTile(
                contentPadding: EdgeInsets.symmetric(horizontal: 15),
                title: Text(
                  task.title,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  ),
                subtitle: Text(
                  task.body,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  ),
                trailing: Icon(Icons.bookmark, color: task.getColor()),
                leading: Checkbox(
                  onChanged: (bool value) {
                    setState(() {
                      task.isChecked = ! task.isChecked; 
                    });
                  },
                  value: task.isChecked,
                ),
              ),
              onTap: (){
                //Navigator.of(context).push(MaterialPageRoute(builder: (context) => CustomDialog(task)));
                /*showDialog(
                  context: context,
                  builder: (BuildContext context) => new CustomDialog(task)                  
                );*/
              }
            )
          )
        )
      ]
    )
  );
}

